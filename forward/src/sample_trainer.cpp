// -*-c++-*-

/*
 *Copyright:

 Copyright (C) Hidehisa AKIYAMA

 This code is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3, or (at your option)
 any later version.

 This code is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this code; see the file COPYING.  If not, write to
 the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.

 *EndCopyright:
 */

/////////////////////////////////////////////////////////////////////

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "sample_trainer.h"
//#include "string.h"
//#include "string"
#include <rcsc/trainer/trainer_command.h>
#include <rcsc/trainer/trainer_config.h>
#include <rcsc/coach/global_world_model.h>
#include <rcsc/common/basic_client.h>
#include <rcsc/common/player_param.h>
#include <rcsc/common/player_type.h>
#include <rcsc/common/server_param.h>
#include <rcsc/param/param_map.h>
#include <rcsc/param/cmd_line_parser.h>
#include <rcsc/random.h>
//#include "fstream"
//#include "iostream"





int SampleTrainer::Forward_Goal_score=0;
int SampleTrainer::Goalie_kick_score=0;
 int  SampleTrainer::Goalie_catch_score=0;
int  SampleTrainer::All_shoot_Number=0;


using namespace rcsc;
using  namespace std;
/*-------------------------------------------------------------------*/
/*!

 */
SampleTrainer::SampleTrainer()
    : TrainerAgent()
{

}

/*-------------------------------------------------------------------*/
/*!

 */
SampleTrainer::~SampleTrainer()
{

}

/*-------------------------------------------------------------------*/
/*!

 */
bool
SampleTrainer::initImpl( CmdLineParser & cmd_parser )
{
    doMoveBall( Vector2D( 52.0, 0.0 ),
                Vector2D( 0.0, 0.0 ) );
    bool result = TrainerAgent::initImpl( cmd_parser );

#if 0
    ParamMap my_params;

    std::string formation_conf;
    my_map.add()
        ( &conf_path, "fconf" )
        ;

    cmd_parser.parse( my_params );
#endif

    if ( cmd_parser.failed() )
    {
        std::cerr << "coach: ***WARNING*** detected unsupported options: ";
        cmd_parser.print( std::cerr );
        std::cerr << std::endl;
    }

    if ( ! result )
    {
        return false;
    }

    //////////////////////////////////////////////////////////////////
    // Add your code here.
    //////////////////////////////////////////////////////////////////

    return true;
}

/*-------------------------------------------------------------------*/
/*!

 */
void
SampleTrainer::actionImpl()
{
    if ( world().teamNameLeft().empty() )
    {
        doTeamNames();
        return;
    }

    //////////////////////////////////////////////////////////////////
    // Add your code here.

    sampleAction();
    recoverForever();
    doSubstitute();
    doKeepaway();
}




/*
 * one by one goalie trainer
 * Kn2c robotic team
 * Mojtaba Moazen
 *
 */
void SampleTrainer::onebyonegoaliee() {

    static std::string status = "init";

    if ( world().gameMode().type() == GameMode::AfterGoal_  || world().gameMode().type() == GameMode::GoalieCatch_  || world().gameMode().type() == GameMode::IndFreeKick_ ||
         world().gameMode().type() == GameMode::FreeKick_  || world().gameMode().type() == GameMode::FreeKick_  )
    {
        status = "move_player_and_ball";
    }




    if (status == "init"){

    }
    else if (status=="move_player_and_ball"){

    }
    else if (status == "kickballtoplayer" ){

    }






}
/*-------------------------------------------------------------------*/
/*!

 */
/*
* one by one goalie trainer
        * Kn2c robotic team
* Mojtaba Moazen
*
*/
void
SampleTrainer::sampleAction()
{
    // to find the number of Goals
    // find best Goalie act

    SampleTrainer::All_shoot_Number++;
    // sample training to test a ball interception.
    static int s_state = 0;
    static int s_wait_counter = 0;

    static Vector2D s_last_player_move_pos;
    if (world().gameMode().type() == GameMode::GoalieCatch_ ||world().gameMode().type() == GameMode::FreeKick_ ){
        SampleTrainer::Goalie_kick_score++;
    }
    else if (world().gameMode().type() == GameMode::AfterGoal_){
        SampleTrainer::Forward_Goal_score++;
    }
//    GameTime gameTime;

//
//
//    ofstream resualt_output_file;
//    resualt_output_file.open ("res.txt", std::ofstream::out | std::ofstream::trunc);
//
//    resualt_output_file << "Goalie_kick_score =>>>>"<<Goalie_kick_score<<endl;
//    resualt_output_file << "Forward_Goal_score =>>>>"<<Forward_Goal_score<<endl;
//    resualt_output_file << "All_shoot_Number =>>>>"<<All_shoot_Number<<endl;
//    resualt_output_file.close();
//
//




    if (world().ball().vel() == Vector2D(0,0)){
        s_state=2;

    }



    if (world().gameMode().type() == GameMode::KickOff_){
       s_state = 1;
    }
    if ( world().gameMode().type() == GameMode::AfterGoal_  || world().gameMode().type() == GameMode::GoalieCatch_  || world().gameMode().type() == GameMode::IndFreeKick_ ||
    world().gameMode().type() == GameMode::FreeKick_  || world().gameMode().type() == GameMode::FreeKick_  )
    {
        s_state = 1;
    }

    switch ( s_state ) {
    case 0:
        // nothing to do
        break;
    case 1:
        // exist kickable left player
        // recover stamina
        doRecover();
        // move ball to center
        doMoveBall( Vector2D( +40.0, 0.0 ),
                    Vector2D( 0.0 , 0.0 ) );
        // change playmode to play_on
        doChangeMode( PM_PlayOn );
        {
            // move player to random point
            UniformReal uni01( 0.0, 1.0 );
//            int random_factor = rand() %10 +1;
//            double random_angle = random_factor * 18 ;
            int min = -34;
            int max = +34;

            double randNum = rand()%(max-min + 1) + min;
            Vector2D move_pos
                = Vector2D(25,randNum);
            s_last_player_move_pos = move_pos;
                Vector2D goalie_move_pos
                        = Vector2D(51.0,0.0);
                s_last_player_move_pos = move_pos;

            doMovePlayer( config().teamName(),
                          2, // uniform number
                          move_pos,
                          move_pos.th() - 180.0 );
            if (!world().teamNameRight().empty()){
                doMovePlayer( world().teamNameRight(),
                              1, // uniform number
                              goalie_move_pos,
                              goalie_move_pos.th() - 180.0 );}

        }
        // change player type
        {
            static int type = 0;
            doChangePlayerType( world().teamNameLeft(), 1, type );
            type = ( type + 1 ) % PlayerParam::i().playerTypes();
        }

        doSay( "move player" );
        s_state = 2;
        std::cout << "trainer: actionImpl init episode." << std::endl;
        break;
    case 2:
        ++s_wait_counter;
        if ( s_wait_counter > 3
             && ! world().playersLeft().empty() )
        {
            // add velocity to the ball
            //UniformReal uni_spd( 2.7, 3.0 );
            //UniformReal uni_spd( 2.5, 3.0 );
            UniformReal uni_spd( 0, 2.7 );
            //UniformReal uni_ang( -50.0, 50.0 );
            UniformReal uni_ang( -10.0, 0 );
            Vector2D velocity
                = Vector2D::polar2vector( 2.5,
                                          (s_last_player_move_pos - Vector2D( +40.0, 0.0) ).dir()
                                           );
            doMoveBall( Vector2D( +40.0, 0.0 ),
                        velocity );
            s_state = 0;
            s_wait_counter = 0;
            std::cout << "trainer: actionImpl start ball" << std::endl;
        }
        break;

    }
}

/*-------------------------------------------------------------------*/
/*!

 */
void
SampleTrainer::recoverForever()
{
    if ( world().playersLeft().empty() )
    {
        return;
    }

    if ( world().time().stopped() == 0
         && world().time().cycle() % 50 == 0 )
    {
        // recover stamina
        doRecover();
    }
}

/*-------------------------------------------------------------------*/
/*!

 */
void
SampleTrainer::doSubstitute()
{
    static bool s_substitute = false;
    if ( ! s_substitute
         && world().time().cycle() == 0
         && world().time().stopped() >= 10 )
    {
        std::cerr << "trainer " << world().time() << " team name = "
                  << world().teamNameLeft()
                  << std::endl;

        if ( ! world().teamNameLeft().empty() )
        {
            UniformSmallInt uni( 0, PlayerParam::i().ptMax() );
            doChangePlayerType( world().teamNameLeft(),
                                1,
                                uni() );

            s_substitute = true;
        }
    }

    if ( world().time().stopped() == 0
         && world().time().cycle() % 100 == 1
         && ! world().teamNameLeft().empty() )
    {
        static int type = 0;
        doChangePlayerType( world().teamNameLeft(), 1, type );
        type = ( type + 1 ) % PlayerParam::i().playerTypes();
    }
}

/*-------------------------------------------------------------------*/
/*!

 */
void
SampleTrainer::doKeepaway()
{
    if ( world().trainingTime() == world().time() )
    {
        std::cerr << "trainer: "
                  << world().time()
                  << " keepaway training time." << std::endl;
    }

}
